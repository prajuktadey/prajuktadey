- 👋 Hi, I’m Prajukta Dey.
- 👀 I’m interested in competitive programming, web development, artificial intelligence and machine learning.
- 🌱 I’m currently learning various programming languages and databases.
- 📫 How to reach me: theprajuktadey@gmail.com

<!---
prajuktadey/prajuktadey is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
